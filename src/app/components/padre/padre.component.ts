import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1 = {
    nombre: "Hary",
    edad: "27",
    oficio:"Cantante"
  };

  Objeto2 = {
    nombre: "Louis",
    edad: "29",
    oficio:"Cantante"
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
